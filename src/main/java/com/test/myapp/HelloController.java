package com.test.myapp;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.test.controller.TestApplicationProperties;
import com.test.dao.UserRepository;
import com.test.dao.Users;



@RestController
public class HelloController {

	TestApplicationProperties appProperty;
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	DataSource dataSource;
	
	private static final Logger logger = LoggerFactory.getLogger(HelloController.class);
	
	public TestApplicationProperties getAppProperty() {
		return appProperty;
	}

	@Autowired
	public void setAppProperty(TestApplicationProperties appProperty) {
		this.appProperty = appProperty;
	}
	
	@RequestMapping(value="/hello1",method=RequestMethod.GET)
	public String sayHello(){
		Iterable<Users> usr=userRepository.findAll();
		for(Users users:usr){
			logger.info("******Hello*********"+users.getUserName());
			logger.info("******Hello*********"+users.getPassword());
		}
		logger.error("******Hello*********"+appProperty.getUserName());
		return "Hello";
	}

}
