package com.test.myapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@org.springframework.boot.autoconfigure.SpringBootApplication
@ComponentScan({"com.test.myapp","com.test.controller"})
@EnableJpaRepositories({"com.test.dao"})
@EntityScan("com.test.dao")   
public class SpringBootApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootApplication.class, args);
	}
}
